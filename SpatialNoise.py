# -*- coding: utf-8 -*-
"""
Created on 26.09.2017 08:30

.. module: 
.. moduleauthor: Matteo Santandrea <matteo.santandrea@upb.de>
"""

import numpy as np
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt

_LONGRANGENOISE = ["quadratic", "quartic", "gaussian", "awgn", "1/f", "1/f2"]


class Noise(object):
    def __init__(self, **kwargs):
        self.z = kwargs.get("z", np.linspace(0, 20, 1000))
        self.offset = kwargs.get("offset", 0.)
        self.long_range_noise = kwargs.get("long_range_noise", "gaussian")
        if not (self.long_range_noise.lower() in _LONGRANGENOISE):
            raise ValueError("long_range_noise variable has to be in {0}".format(_LONGRANGENOISE))
        self.long_range_parameter = kwargs.get("long_range_parameter")
        if self.long_range_noise.lower() in _LONGRANGENOISE[:3]:
            self.short_range_noise = kwargs.get("short_range_noise", "gaussian")
            if not self.short_range_noise.lower() == "gaussian":
                raise NotImplementedError("This short noise has not yet been implemented")
            self.short_range_parameter = kwargs.get("short_range_parameter")
            self.n_points_long_range = kwargs.get("number_points_long_range", 10)
            self.n_points_short_range = kwargs.get("number_points_short_range", 300)
            self.noise = self.generate_long_range_noise() + self.generate_short_range_noise() + self.offset
        else:
            self.noise = self.generate_noise() + self.offset
        self.calculate_autocorrelation()

    def generate_noise(self):
        length = self.z[-1] - self.z[0]
        npoints = len(self.z)
        if npoints % 2 == 0:
            npoints += 1
            extended = True
        else:
            extended = False
        df = 1. / length
        C = np.zeros(shape=npoints, dtype=complex)
        half_size = int((npoints - 1) / 2)

        if self.long_range_noise == "AWGN":
            exponent = 0
        elif self.long_range_noise == "1/f":
            exponent = 1
        elif self.long_range_noise == "1/f2":
            exponent = 2
        else:
            raise ValueError("Unknown self.long_range_noise value. Cannot set the exponent.")

        for idx in range(1, half_size + 1):
            fk = idx * df / (2 * np.pi)
            ck = 1. / fk ** exponent
            phase = np.random.uniform(0, 2 * np.pi)
            C[half_size + idx] = ck * np.exp(1j * phase)
            C[half_size - idx] = ck * np.exp(-1j * phase)
        y = np.fft.ifft(np.fft.ifftshift(C))
        y *= self.long_range_parameter / abs(y).max()
        if extended:
            y = y[:-1]
        return np.real(y)

    def generate_short_range_noise(self):
        if self.short_range_noise.lower() == "gaussian":
            self.short_range = np.random.randn(len(self.z)) * self.short_range_parameter
        else:
            raise NotImplementedError("This short range has not yet been implemented")
        return self.short_range

    def generate_long_range_noise(self):
        xx = np.linspace(0, self.z.max(), self.n_points_long_range)
        if self.long_range_noise.lower() == "gaussian":
            long_range = np.random.randn(self.n_points_long_range) * self.long_range_parameter
        elif self.long_range_noise.lower() == "quadratic":
            points = np.linspace(-1, 1, self.n_points_long_range)
            long_range = points ** 2 * self.long_range_parameter
        else:
            raise NotImplementedError("This long range has not yet been implemented")
        # interpolate for the short mesh
        y = interp1d(xx, long_range, kind="cubic", fill_value="extend")
        return y(self.z)

    def calculate_noise(self, **kwargs):
        noise = self.generate_long_range_noise() + self.generate_short_range_noise() + self.offset
        if kwargs.get("plot", False):
            plt.figure()
            plt.plot(self.z, noise)
        return noise

    def calculate_autocorrelation(self):
        # n = len(self.noise)
        # variance = self.noise.var()
        # x = self.noise - self.noise.mean()
        # r = np.correlate(x, x, mode='full')[-n:]
        # # assert np.allclose(r, np.array([(x[:n-k]*x[-(n-k):]).sum() for k in range(n)]))
        # result = r / (variance * (np.arange(n, 0, -1)))
        self.f = np.linspace(-1. / (2 * (self.z[1] - self.z[0])), 1. / (2 * (self.z[1] - self.z[0])), len(self.z))
        FR = np.fft.fft(self.noise)
        self.power_spectrum = FR * np.conj(FR)
        self.autocorrelation = np.fft.ifftshift(np.fft.ifft(self.power_spectrum))
        self.power_spectrum = np.fft.fftshift(self.power_spectrum)
        return self.autocorrelation, self.power_spectrum

    def plot_noise_properties(self):
        if self.autocorrelation is None:
            self.calculate_autocorrelation()
        plt.figure()
        plt.subplot(211)
        plt.plot(self.z, self.noise)
        plt.title("Noise")
        plt.xlabel("z")
        plt.ylabel("Noise")
        plt.subplot(223)
        plt.semilogy(np.linspace(-self.z.max() / 2., self.z.max() / 2., len(self.z)), abs(self.autocorrelation) ** 2)
        plt.title("|R(z)|^2")
        plt.xlabel("z")
        plt.subplot(224)
        plt.semilogy(self.f, abs(self.power_spectrum) ** 2)
        plt.title("|S(f)|^2")
        plt.xlabel("f")
        plt.tight_layout()


if __name__ == "__main__":
    # thisnoise = Noise(short_range_parameter=0.01, long_range_parameter=0.1)
    # thisnoise.generate_short_range_noise()
    # thisnoise.generate_long_range_noise()
    # thisnoise.calculate_noise()
    # thisnoise.calculate_autocorrelation()
    # thisnoise.plot_noise_properties()

    thisnoise = Noise(long_range_noise="AWGN", long_range_parameter=0.1, offset=3.0)
    thisnoise.plot_noise_properties()
    plt.show()
