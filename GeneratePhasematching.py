import numpy as np
import matplotlib.pyplot as plt
import SpatialNoise as SN
import sellmeier_all as sellm


class Phasematching(object):
    def __init__(self, z, wlr, T0=25, Pin=0.01, verbose = False):
        # Define the space mesh
        self.z = z  # in mm

        # To keep things simple, calculate the phasematching
        # - without the width information;
        # - SHG type 0;
        self.n, _, _ = sellm.LiNbO3(typ="disptemp")
        self.T0 = T0
        self.wlr = wlr
        etanorm = 49  # %/W cm^2
        self.Pin = Pin
        self.eta = etanorm / 1e4 * self.Pin
        # Calculate the poling with the central value of the wlr
        self.poling = 2 * np.pi / self.deltabeta_temperature(self.T0, self.wlr.mean())
        if verbose:
            print("The poling period for SHG type 0 from FF %4.2fnm is %2.2f um" % (self.wlr.mean() * 1e3, self.poling))
        # ideal deltabeta as a function of the temperature.
        self.deltabeta_T = lambda T: self.deltabeta_temperature(T, self.wlr, poling0=self.poling)
        # generate a noise profile. Initialized with 0 noise.
        self.thisnoise = SN.Noise(z=self.z,
                                  long_range_noise="1/f",
                                  long_range_parameter=0.)
        self.phi = None

    def deltabeta_temperature(self, T, wlff, poling0=1E5, thermal_expansion=True, sellmeier_temp=True):
        alpha_a_1 = 0.154e-4  # 1/K
        alpha_a_2 = 0.053E-7  # 1/K2
        if thermal_expansion:
            poling = poling0 * (1 + alpha_a_1 * (T - self.T0) + alpha_a_2 * (T - self.T0) ** 2)
        else:
            poling = poling0
        if sellmeier_temp:
            N = self.n(wlff / 2., T) / (wlff / 2.) - 2 * self.n(wlff, T) / wlff
        else:
            N = self.n(wlff / 2., self.T0) / (wlff / 2.) - 2 * self.n(wlff, self.T0) / wlff
        return 2 * np.pi * (N - 1 / poling)

    def add_noise(self, noise_type="1/f", noise_param=0.0005):
        self.thisnoise = SN.Noise(z=self.z,
                                  long_range_noise=noise_type,
                                  long_range_parameter=noise_param)

    def evaluate_phasematching(self, temperature=None, normalized=True):

        DELTABETA, _ = np.meshgrid(self.deltabeta_T(self.T0), self.z)
        if temperature is not None:
            if temperature.shape != self.z.shape:
                raise IOError("temperature.shape does not match with self.z.shape")
            else:
                for idx, z in enumerate(self.z):
                    DELTABETA[idx, :] = self.deltabeta_T(temperature[idx])

        if self.thisnoise.long_range_parameter != 0.0:
            for row in range(DELTABETA.shape[0]):
                DELTABETA[row, :] += self.thisnoise.noise[row]

        cum_dbeta = np.zeros(shape=(DELTABETA.shape[1],))
        cum_exp = np.zeros(shape=(DELTABETA.shape[1],), dtype=complex)
        Z = self.z * 1e3
        dz = Z[1] - Z[0]

        for idx, z in enumerate(Z):
            delta_beta = DELTABETA[idx, :]
            cum_dbeta += delta_beta
            cum_exp += np.exp(1j * dz * cum_dbeta)
        phi = dz / Z[-1] * cum_exp
        self.phi = phi
        if not normalized:
            self.phi *= np.sqrt(self.eta)
        self.intensity = abs(phi) ** 2

    def plot_phi(self, ax=None, title=None, xlabel=None, ylabel=None, ls="-", label=None):
        if self.phi is None:
            raise AttributeError("self.phi is None")
        if ax is None:
            plt.figure()
            ax = plt.gca()

        ax.plot(self.wlr * 1e3, self.intensity, ls=ls)
        return ax


if __name__ == "__main__":
    L = 40  # mm
    dz = 0.05  # mm
    z = np.arange(0, L + dz, dz)

    wl0 = 1.55
    dwl = 0.01
    wlr = np.linspace(wl0 - dwl / 2., wl0 + dwl / 2., 1000)

    # IDEAL, NOISELESS PM
    noiselessphasematching = Phasematching(z, wlr)
    noiselessphasematching.evaluate_phasematching(normalized=False)
    ax = noiselessphasematching.plot_phi(label="Ideal")

    # NOISY PM
    noisyphasematching = Phasematching(z, wlr)
    noisyphasematching.add_noise()
    noisyphasematching.evaluate_phasematching(normalized=False)
    noisyphasematching.plot_phi(ax=ax, label="Noisy")

    # TEMP DEPENDENT PM
    temperature_phasematching = Phasematching(z, wlr)
    temp_profile = SN.Noise(z=z,
                            long_range_noise="1/f",
                            offset=25,
                            long_range_parameter=5)
    temp_profile.plot_noise_properties()
    temperature_phasematching.evaluate_phasematching(temperature=temp_profile.noise, normalized=False)
    temperature_phasematching.plot_phi(ax=ax, label="Temp variable")

    # TEMP + NOISE PM
    thispm = Phasematching(z, wlr)
    thispm.add_noise()
    thispm.evaluate_phasematching(temperature=temp_profile.noise, normalized=False)
    thispm.plot_phi(ax=ax, label="Temp variable")

    plt.show()
